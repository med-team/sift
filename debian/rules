#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
export DH_VERBOSE = 1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic


PACKAGE:=sift
DESTDIR:=$(CURDIR)/debian/$(PACKAGE)
VERSION:=6.2.1
prefix:=/usr
pkgdatadir:=$(prefix)/share/$(PACKAGE)

SIFT_BINDIR := $(prefix)/lib/sift/bin
SIFT_SCRIPTDIR := $(prefix)/lib/sift/bin

parallel := 1
$(eval $(DEB_BUILD_OPTIONS))

%:
	dh $@

MAN1:=info_on_seqs.1 SIFT_for_submitting_fasta_seq.csh.1

.PHONY: man
man: $(MAN1)

%.1: %.pod
	pod2man -c 'User Commands' --release="$(VERSION)" $< > $@

override_dh_auto_clean:
	if [ -e src/Makefile -a -e src/deps.mk ]; then make -j$(parallel) -C src prefix=$(prefix) distclean; fi
	rm -f $(MAN1)
	# remove pre-compiled binaries from bin - thanks to Michael Bienia <geser@ubuntu.com>
	rm -f bin/choose_seqs_via_psiblastseedmedian
	rm -f bin/clump_output_alignedseq
	rm -f bin/consensus_to_seq
	rm -f bin/fastaseqs
	rm -f bin/info_on_seqs
	rm -f bin/process_alignment
	rm -f bin/psiblast_res_to_fasta_dbpairwise
	rm -f bin/seqs_from_psiblast_res
	rm -f src/choose_seqs_via_psiblastseedmedian
	rm -f src/clump_output_alignedseq
	rm -f src/consensus_to_seq
	rm -f src/deps.mk
	rm -f src/info_on_seqs
	rm -f src/psiblast_res_to_fasta_dbpairwise
	rm -f src/seqs_from_psiblast_res
	find . -name "*.o" -delete

override_dh_auto_build: man
	make -j$(parallel) -C src prefix=$(prefix) VERSION=$(VERSION) deps.mk && \
	make -j$(parallel) -C src prefix=$(prefix) VERSION=$(VERSION)

override_dh_auto_install:
	make -C src DESTDIR=$(DESTDIR) prefix=$(prefix) install

override_dh_install:
	# lkajan: fastaseqs comes from the blimps package, this source does not have rules for creating it
	dh_install -X.svn -X.swp -XIntersectFeatures.jar -XManuals -Xfastaseqs -Xlinux -Xsolaris bin $(prefix)/lib/sift/
	#dh_install bin/IntersectFeatures.jar $(pkgdatadir)/
	dh_install -X.svn -X.swp blimps/docs $(prefix)/share/sift/blimps/

	#Links are created by debian/links
	for f in SIFT_for_submitting_fasta_seq.csh SIFT_for_submitting_NCBI_gi_id.csh; do \
		sed --in-place -e 's|\b__MAKE_PREFIX__\b|$(prefix)|g;s|__SIFT_SCRIPTDIR__|$(SIFT_SCRIPTDIR)|g;s|__SIFT_BINDIR__|$(SIFT_BINDIR)|g;' $(DESTDIR)$(SIFT_SCRIPTDIR)/$$f; \
	done;

	for f in seqs_chosen_via_median_info.csh \
	         SIFT_for_submitting_fasta_seq.csh \
		 SIFT_for_submitting_NCBI_gi_id.csh; \
	do \
		sed --in-place -e 's|\b__MAKE_PREFIX__\b|$(prefix)|g;s|__SIFT_SCRIPTDIR__|$(SIFT_SCRIPTDIR)|g;s|__SIFT_BINDIR__|$(SIFT_BINDIR)|g;' $(DESTDIR)$(SIFT_SCRIPTDIR)/$$f; \
	done;

	# look out: DNA_PROT_SUBROUTINES.pl is really a module (should be pm)!
	# Update: Not existing at all in 6.2.1
	for f in SIFT_subroutines.pm \
		 DNA_PROT_SUBROUTINES.pl \
		 perlscripts/get_BLINK_seq.pl \
		 perlscripts/separate_query_from_rest_of_seqs.pl \
		 perlscripts/separate_query_from_database.pl; \
	do \
		ff="$(DESTDIR)$(prefix)/lib/sift/bin/$$f"; \
		if [ -r "$$ff" ]; then \
			sed -i '1s%/usr/local/bin/%/usr/bin/%' $$ff; \
			chmod +x $$ff; \
		fi; \
	done;

	for f in IntersectFeatures.jar; do \
		if [ -r "$(DESTDIR)$(pkgdatadir)/$$f" ]; then \
			chmod -x $(DESTDIR)$(pkgdatadir)/$$f; \
		fi; \
	done;
